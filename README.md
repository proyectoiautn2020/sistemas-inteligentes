# Sistemas Inteligentes

Este repositorio contiene el código para el desarrollo de las redes neuronales backpropagation del trabajo práctico N°1 de Sistemas Inteligentes de la materia Inteligencia Artificial de la UTN.

El objetivo del mismo es identificar si una planta de cannabis medicinal se encuentra en "buen estado" o "mal estado" en base a una foto de la planta.

#### Descripción de los archivos

Se realizaron 3 modelos con distintas configuraciones para poder lograr el objetivo propuesto para el trabajo.

-  `train_N.py`: contienen el código de la red neuronal.
-  `train_N_output.txt`: contienen la salida en consola del entrenamiento de la red en base a la ejecución del archivo  `train_N.py`.
-  `test_N_output.txt`: contiene la salida en consola de la ejecución del archivo  `predict.py` con la previa configuración interna del tamaño de imagen en base al modelo probado.