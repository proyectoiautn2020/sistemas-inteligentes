import numpy as np
import tensorflow as tf
from keras.preprocessing.image import load_img, img_to_array
from keras.models import load_model
from keras.utils import CustomObjectScope

width, height = 80, 80
modelo = './modelo/modelo.h5'
pesos_modelo = './modelo/pesos.h5'
# with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
# cnn = load_model(modelo)
cnn = tf.keras.models.load_model(modelo)
cnn.load_weights(pesos_modelo)


def predict(file):
    x = load_img('test/' + file, target_size=(width, height), grayscale=True)
    x = img_to_array(x)
    x = np.expand_dims(x, axis=0)
    array = cnn.predict(x)
    print(array)
    result = array[0]
    answer = np.argmax(result)
    if answer == 0:
        print(file + " - Buen estado")
    elif answer == 1:
        print(file + " - Mal estado")

    return answer


photos = ["buen_estado/foto1.jpg", "buen_estado/foto2.jpg", "buen_estado/foto3.jpg", "mal_estado/foto4.jpg",
          "mal_estado/foto5.jpg", "mal_estado/foto6.jpg"]
for photo in photos:
    predict(photo)
