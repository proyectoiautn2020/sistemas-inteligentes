import os
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
from tensorflow.python.keras import optimizers
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dropout, Flatten, Dense
from tensorflow.python.keras import backend as K
from tensorflow import keras
import tensorflow as tf

K.clear_session()
tf.compat.v1.disable_eager_execution()

data_entrenamiento = './data/entrenamiento'
data_validacion = './data/validacion'

"""
Parameters
"""
epochs = 10
width, height = 150, 150
batch_size = 10
steps = 500
validation_steps = 100
classes = 2
learning_rate = 0.005

# Preparacion de las imagenes

entrenamiento_datagen = ImageDataGenerator(
    rotation_range=40,
    width_shift_range=0.2,
    height_shift_range=0.2,
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest')

test_datagen = ImageDataGenerator(rescale=1. / 255)

entrenamiento_generador = entrenamiento_datagen.flow_from_directory(
    data_entrenamiento,
    target_size=(height, width),
    color_mode="grayscale",
    batch_size=batch_size,
    class_mode='binary')

validacion_generador = test_datagen.flow_from_directory(
    data_validacion,
    target_size=(height, width),
    color_mode="grayscale",
    batch_size=batch_size,
    class_mode='binary')


# Configuracion de la red neuronal
model = Sequential()

model.add(Flatten(input_shape=(width, height, 1)))
model.add(Dense(2048, activation='relu'))
model.add(Dense(1024, activation='relu'))
model.add(Dense(1024, activation='relu'))
model.add(Dense(1024, activation='relu'))
model.add(Dense(1024, activation='relu'))
model.add(Dense(1024, activation='relu'))
model.add(Dense(256, activation='relu'))

model.add(Dropout(0.5))
model.add(Dense(classes, activation='softmax'))

model.compile(loss='sparse_categorical_crossentropy',
              optimizer=optimizers.Adam(lr=learning_rate),
              metrics=['accuracy', 'binary_accuracy', 'categorical_accuracy'])

model.summary()

# Criterio de finalizacion temprana en caso de que no mejore el modelo durante varias epochs
early_stopping_cb = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=3, verbose=0, mode='auto')

# Ejecutar entrenamiento
model.fit(
    entrenamiento_generador,
    steps_per_epoch=steps,
    epochs=epochs,
    validation_data=validacion_generador,
    validation_steps=validation_steps,
    callbacks=[early_stopping_cb])

# Guardar modelo
target_dir = './modelo/'
if not os.path.exists(target_dir):
    os.mkdir(target_dir)
model.save('./modelo/modelo.h5')
model.save_weights('./modelo/pesos.h5')

print(entrenamiento_generador.class_indices)
